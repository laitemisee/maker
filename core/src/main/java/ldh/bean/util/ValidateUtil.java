package ldh.bean.util;

public class ValidateUtil {

	public static boolean notNull(String str) {
		if (str == null) {
			return false;
		}
		return true;
	}
	
	public static boolean notEmpty(String str) {
		if (str != null && !str.trim().equals("")) {
			return true;
		}
		return false;
	}
	
	public static boolean isEmpty(String str) {
		if (str != null && !str.trim().equals("")) {
			return false;
		}
		return true;
	}
	
	public static boolean notNull(Object obj) {
		if (obj == null) {
			return false;
		}
		return true;
	}
	
	public static boolean notNull(Object... obj) {
		if (obj == null) {
			return false;
		} else {
			for (Object ob : obj) {
				if (!notNull(ob)) return false;
			}
		}
		
		return true;
	}
}
