<template>
  <div>
    <table-view :headColumns="headColumns" :rowDatas="rowDatas" v-bind:total="total"
                v-bind:pageSize="pageSize" v-bind:currentPageNum="currentPageNum" v-bind:pageNumShow="pageNumShow"
                v-bind:pageSizeList="pageSizeList" v-on:loadData="loadData"></table-view>
  </div>
</template>

<script>
  import TableView from '@/components/TableView'

  export default {
    name: '${util.firstLower(table.javaName)}-list',
    components: {
      "table-view": TableView
    },
    mounted: function() {
        this.initData(1, 10);
    },
    data() {
        return {
            headColumns: [
            <#list table.columnList as column>
              <#if column.create>
                <#if column.foreign>
                    {'label': '${column.property}.${column.foreignKey.foreignTable.columnList[0].property}', 'field': '${util.comment(column)}'<#if column.width??>, 'width':${column.width}</#if>},
                <#else>
                    {'label': '${util.comment(column)}', 'field': '${column.property}'<#if column.width??>, 'width': ${column.width}</#if>},
                </#if>
              </#if>
            </#list>
                {'label': '操作', 'field': 'opt', 'width': 200}
            ],
            rowDatas: [],
            total: 0, pageSize: 10, currentPageNum: 1, pageNumShow: 10, pageSizeList: [10, 20, 30, 50, 100]
        }
    },
    methods: {
      loadData:function(pageNum, pageSize){
        this.initData(pageNum, pageSize);
      },
      initData:function(pageNum, pageSize) {
        var that=this;
        $.ajax({
          type:"get",
          data: {"pageNo": pageNum, "pageSize": pageSize},
          dataType:"JSON",
          crossDomain: true,
          url:"http://localhost:8282/${util.firstLower(table.javaName)}/list/json",
          success:function(res){
            that.rowDatas=that.addOper(res.data.beans);
            that.total = res.data.total;
            that.currentPageNum = res.data.pageNo
            that.pageSize = res.data.pageSize;
          }
        })
      },
      addOper:function(data) {
        var that=this;
          for(var dd in data) {
            console.info(data[dd])
              data[dd].opt = "<a href=/#/${util.firstLower(table.javaName)}/view?id=" + data[dd].id  + ">编辑</a>";
          }
          return data;
      }
    }
  }
</script>
