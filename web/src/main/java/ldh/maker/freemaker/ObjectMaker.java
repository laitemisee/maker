package ldh.maker.freemaker;

/**
 * Created by ldh123 on 2018/6/30.
 */
public class ObjectMaker extends BeanMaker<ObjectMaker>{

    public ObjectMaker beanPackage(String beanPackage) {
        this.pack(beanPackage);
        return this;
    }

    public ObjectMaker put(String key, Object value) {
        data.put(key, value);
        return this;
    }

    public void data() {
        className = fileName.substring(0, fileName.indexOf("."));
        super.data();
        check();
        data.put("beanPackage", pack);
    }

    @Override
    public ObjectMaker make() {
        data();
        out(ftl, data);

        return this;
    }
}
